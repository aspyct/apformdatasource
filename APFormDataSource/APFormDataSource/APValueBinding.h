//
//  APValueBinding.h
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APValueBinding : NSObject

- (void)bindProperty:(NSString *)property ofObject:(NSObject *)object;
- (void)unbindObject:(NSObject *)object;

@end
