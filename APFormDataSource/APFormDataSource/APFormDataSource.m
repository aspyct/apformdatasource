//
//  APFormDataSource.m
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import "APFormDataSource.h"

@implementation APFormDataSource {
    NSMutableArray *_groups;
}

- (id)init
{
    self = [super init];
    
    if (self) {
        _groups = [[NSMutableArray alloc] initWithCapacity:5];
    }
    
    return self;
}

- (void)addGroup:(APFormFieldGroup *)group
{
    [_groups addObject:group];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return _groups.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self groupAt:section].numberOfFields;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return [self groupAt:section].name;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self groupAt:indexPath.section] cellForRow:indexPath.row];
}

- (APFormFieldGroup *)groupAt:(NSInteger)section
{
    return [_groups objectAtIndex:section];
}

@end
