//
//  APFormGroup.h
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "APFormField.h"

@interface APFormFieldGroup : NSObject

@property (strong, nonatomic) NSString *name;
@property (readonly) NSInteger numberOfFields;

- (id)initWithName:(NSString *)name;
- (UITableViewCell *)cellForRow:(NSUInteger)row;
- (void)addField:(APFormField *)field;

@end
