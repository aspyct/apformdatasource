//
//  APTextFormField.h
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import "APFormField.h"

@interface APTextFormField : APFormField

@property (strong, nonatomic) NSString *label;
@property (strong, nonatomic) NSString *placeholder;

@end
