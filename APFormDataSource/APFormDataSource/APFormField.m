//
//  APFormField.m
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import "APFormField.h"

@implementation APFormField {
    __weak NSObject *_target;
    NSString *_property;
}

@synthesize target=_target;
@synthesize property=_property;

- (id)initWithBinding:(NSObject *)target onProperty:(NSString *)property
{
    self = [super init];
    
    if (self) {
        _target = target;
        _property = property;
    }
    
    return self;
}

- (UITableViewCell *)cell
{
    [NSException raise:@"APFormField is abstract" format:@"Please subclass me"];
    return nil; // Stupid warning...
}

@end
