//
//  APFormField.h
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APFormField : NSObject

@property (readonly) UITableViewCell *cell;
@property (readonly) NSObject *target;
@property (readonly) NSString *property;

- initWithBinding:(NSObject *)target onProperty:(NSString *)property;

@end
