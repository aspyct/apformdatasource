//
//  APFormDataSource.h
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "APFormFieldGroup.h"

@interface APFormDataSource : NSObject<UITableViewDataSource>

- (void)addGroup:(APFormFieldGroup *)group;

@end
