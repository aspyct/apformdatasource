//
//  APTextFormField.m
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import "APTextFormField.h"
#import <objc/runtime.h>

#import "APValueBinding.h"

const static void *APTextFormFieldBinding;

@implementation APTextFormField

- (UITableViewCell *)cell
{
    // TODO Reuse cells, but this should not be too much of a performance issue
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"APTextFormCell"];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 100, 34)];
    [cell.contentView addSubview:titleLabel];
    
    UITextField *textField = [[UITextField alloc] initWithFrame:CGRectMake(120, 5, 200, 34)];
    [cell.contentView addSubview:textField];
    
    titleLabel.text = self.label;
    textField.placeholder = self.placeholder;
    
    APValueBinding *binding = [[APValueBinding alloc] init];
    [binding bindProperty:self.property ofObject:self.target];
    [binding bindProperty:@"text" ofObject:textField];
    objc_setAssociatedObject(cell, APTextFormFieldBinding, binding, OBJC_ASSOCIATION_RETAIN);
    
    return cell;
}

@end
