//
//  main.m
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "APAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([APAppDelegate class]));
    }
}
