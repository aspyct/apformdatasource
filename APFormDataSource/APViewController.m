//
//  APViewController.m
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import "APViewController.h"

#import "APFormDataSource.h"
#import "APFormFieldGroup.h"
#import "APTextFormField.h"

@interface APViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *theValue;

@end

@implementation APViewController {
    APFormDataSource *_ds;
}
- (IBAction)doIt:(id)sender {
    [self.view endEditing:YES];
    NSLog(@"%@", self.theValue);
}

- (void)viewDidLoad
{
    self.theValue = @"Hello";
    
    _ds = [[APFormDataSource alloc] init];
    
    APFormFieldGroup *group = [[APFormFieldGroup alloc] initWithName:@"Default"];
    APTextFormField *field = [[APTextFormField alloc] initWithBinding:self onProperty:@"theValue"];
    field.label = @"Your name";
    field.placeholder = @"Antoine";
    
    [group addField:field];
    [group addField:field];
    [_ds addGroup:group];
    
    self.tableView.dataSource = _ds;
    [self.tableView reloadData];
}

@end
