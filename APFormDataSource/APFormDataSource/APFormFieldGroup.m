//
//  APFormGroup.m
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import "APFormFieldGroup.h"

@implementation APFormFieldGroup {
    NSMutableArray *_fields;
}

- (id)initWithName:(NSString *)name
{
    self = [super init];
    
    if (self) {
        self.name = name;
        _fields = [[NSMutableArray alloc] initWithCapacity:5];
    }
    
    return self;
}

- (NSInteger)numberOfFields
{
    return _fields.count;
}

- (void)addField:(APFormField *)field
{
    [_fields addObject:field];
}

- (UITableViewCell *)cellForRow:(NSUInteger)row
{
    return [self fieldAt:row].cell;
}

- (APFormField *)fieldAt:(NSInteger)row
{
    return [_fields objectAtIndex:row];
}

@end
