//
//  APValueBinding.m
//  APFormDataSource
//
//  Created by aspyct on 15/01/13.
//  Copyright (c) 2013 aspyct. All rights reserved.
//

#import "APValueBinding.h"

@implementation APValueBinding {
    __weak NSObject *_objectA;
    NSString *_propertyA;
    
    __weak NSObject *_objectB;
    NSString *_propertyB;
    
    __weak NSObject *_ignore;
}

- (void)bindProperty:(NSString *)property ofObject:(NSObject *)object
{
    if (_objectA == nil) {
        _objectA = object;
        _propertyA = property;
    }
    else if (_objectB == nil) {
        _objectB = object;
        _propertyB = property;
    }
    else {
        [NSException raise:@"Binding is full" format:@"Cannot bind more than two objects"];
    }
    
    [object addObserver:self forKeyPath:property options:(NSKeyValueObservingOptionNew) context:nil];
}

- (void)unbindObject:(NSObject *)object
{
    if (object == _objectA) {
        _objectA = nil;
        _propertyA = nil;
    }
    else if (object == _objectB) {
        _objectB = nil;
        _propertyB = nil;
    }
    else {
        [NSException raise:@"Not observed" format:@"The given object was not observed"];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (object == _ignore) {
        _ignore = nil;
    }
    else {
        id newValue = [change objectForKey:NSKeyValueChangeNewKey];
        
        if (object == _objectA) {
            _ignore = _objectB;
            [_objectB setValue:newValue forKey:_propertyB];
        }
        else if (object == _objectB) {
            _ignore = _objectA;
            [_objectA setValue:newValue forKey:_propertyA];
        }
    }
}

@end
